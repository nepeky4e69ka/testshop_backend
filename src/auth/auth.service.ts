import {
  Injectable,
  // UnauthorizedException,
  NotFoundException,
  ConflictException,
  BadRequestException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { config } from '../app.config';
import { UserService } from '../user/user.service';
import { AuthSignDto } from './auth.sign.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}
  verify(token: string) {
    return this.jwtService.verify(token, { secret: config.secret });
  }
  async signup({ email, password }: AuthSignDto): Promise<{ token: string }> {
    const exist = await this.userService.getUser(email);
    if (exist) throw new ConflictException('User exist');
    await this.userService.create({ email, password });
    const token = this.jwtService.sign({ email });
    return { token };
  }
  async signin({ email, password }: AuthSignDto): Promise<{ token: string }> {
    const exist = await this.userService.getUser(email);
    if (!exist) throw new NotFoundException('User not exist');
    const valid = await this.userService.validatePassword({ email, password });
    if (!valid) throw new BadRequestException('Wrong password');
    const token = this.jwtService.sign({ email });
    return { token };
  }
}
