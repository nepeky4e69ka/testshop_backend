import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { TokenPayload } from '../auth/auth.sign.dto';
import { config } from '../app.config';
import { UserInterface } from '../user/user.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.secret,
      expiresIn: '2s',
    });
  }
  async validate({ email, exp }: TokenPayload): Promise<UserInterface> {
    const user = await this.userService.getUser(email);
    const expired = exp * 1000 < new Date().getTime();
    if (!user || expired) {
      throw new UnauthorizedException('Invalid token', '401');
    }
    return user;
  }
}
