import { Controller, Post, Body } from '@nestjs/common';
import { AuthSignDto } from './auth.sign.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @Post('/signup')
  async signup(@Body() body: AuthSignDto) {
    return await this.authService.signup(body);
  }
  @Post('/signin')
  async signin(@Body() body: AuthSignDto) {
    return await this.authService.signin(body);
  }
}
