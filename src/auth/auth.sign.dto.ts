import { IsEmail, IsNotEmpty } from 'class-validator';

export class AuthSignDto {
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;
  readonly password?: string;
}
export class TokenPayload extends AuthSignDto {
  readonly exp: number;
}
