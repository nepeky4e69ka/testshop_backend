export interface UserInterface {
  _id?: any;
  email: string;
  password: string;
  salt: string;
}
