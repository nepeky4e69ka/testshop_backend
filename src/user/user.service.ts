import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { User } from './user.schemas';
import { ReturnModelType } from '@typegoose/typegoose';
import { AuthSignDto } from '../auth/auth.sign.dto';
import * as bcrypt from 'bcrypt-nodejs';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User)
    private readonly userModel: ReturnModelType<typeof User>,
  ) {}
  async create({ email, password }: AuthSignDto) {
    const salt = bcrypt.genSaltSync();
    const crypted = await this.hashPassword(password, salt);
    return await new this.userModel({ email, salt, password: crypted }).save();
  }
  async getUser(email: string): Promise<User> {
    return await this.userModel.findOne({ email }, '-salt -password');
  }
  async hashPassword(password: string, salt: string): Promise<string> {
    return await bcrypt.hashSync(password, salt);
  }
  async validatePassword({ email, password }: AuthSignDto): Promise<boolean> {
    const user = await this.userModel.findOne({ email }, 'salt password');
    if (!user) throw new NotFoundException('User not found');
    const hash = await this.hashPassword(password, user.salt);
    return hash === user.password;
  }
}
