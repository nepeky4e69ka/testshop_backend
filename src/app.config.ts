export const config = {
  mongodb: 'mongodb://localhost:27017/testshop?replicaSet=rs',
  secret: 'someSuperSecretWordInTheWorld',
  static: `${process.cwd()}/static`,
  allowedUploadFileTypes: ['image/jpeg', 'image/jpg', 'image/png'],
  staticPrefix: { prefix: '/static/' },
};
