import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { MediaModule } from './media/media.module';
import { ProductModule } from './product/product.module';
import { config } from './app.config';

@Module({
  imports: [
    TypegooseModule.forRoot(config.mongodb),
    AuthModule,
    UserModule,
    MediaModule,
    ProductModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
