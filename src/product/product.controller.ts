import { AuthGuard } from '@nestjs/passport';
import { ProductService } from './product.service';
import { ProductQueryDto, ProductIdDto } from './product.dto';
import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Delete,
  Query,
  UseGuards,
  Param,
} from '@nestjs/common';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}
  @Get()
  @UseGuards(AuthGuard('jwt'))
  async products(@Query() query: ProductQueryDto) {
    return await this.productService.products(query);
  }
  @Post()
  @UseGuards(AuthGuard('jwt'))
  async create(@Body() body) {
    return await this.productService.create(body);
  }
  @Put()
  @UseGuards(AuthGuard('jwt'))
  async update(@Body() body) {
    return await this.productService.update(body);
  }
  @Delete(':_id')
  @UseGuards(AuthGuard('jwt'))
  async delete(@Param() params: ProductIdDto) {
    return await this.productService.delete(params);
  }
}
