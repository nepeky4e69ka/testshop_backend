import { Type, Transform } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

class ProductQuerySort {
  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  readonly created: number;
  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  readonly price: number;
}

export class ProductQueryDto {
  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  readonly limit: number;

  @IsNotEmpty()
  @IsNumber()
  @Type(() => Number)
  readonly skip: number;

  @Transform(({ value }) => JSON.parse(value))
  readonly sort!: ProductQuerySort;
}

export class ProductIdDto {
  @IsString()
  @IsNotEmpty()
  readonly _id: string;
}
