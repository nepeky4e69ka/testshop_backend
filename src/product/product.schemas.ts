import { prop, Ref, pre } from '@typegoose/typegoose';
import { Media } from '../media/media.schemas';

@pre<Product>('save', async function (next) {
  const count = await this.collection.countDocuments();
  this.article = ('00000' + (1 + count)).slice(-5);
  this.created = new Date().getTime();
  next();
})
export class Product {
  @prop()
  article: string;

  @prop()
  created: number;

  @prop({ required: true })
  name: string;

  @prop({ required: true })
  price: number;

  @prop({ required: true, autopopulate: true, ref: Media })
  medias: Ref<Media>[];
}
