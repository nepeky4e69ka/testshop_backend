import { Injectable, Logger } from '@nestjs/common';
import { ClientSession } from 'mongoose';
import { InjectModel } from 'nestjs-typegoose';
import { Product } from './product.schemas';
import { ProductInterface, ProductResponse } from './product.interface';
import { ReturnModelType } from '@typegoose/typegoose';
import { ProductQueryDto } from './product.dto';

@Injectable()
export class ProductService {
  private logger: Logger = new Logger('ProductService');
  constructor(
    @InjectModel(Product)
    private readonly productModel: ReturnModelType<typeof Product>,
  ) {}
  async products({
    limit,
    skip,
    sort,
  }: ProductQueryDto): Promise<ProductResponse> {
    const products = await this.productModel
      .find({}, null, { limit, skip, sort })
      .populate('medias');
    const count = await this.productModel.countDocuments();
    const isMoreAvaliable = count > limit + skip;
    return { products, isMoreAvaliable };
  }
  async create(payload: Partial<ProductInterface>) {
    return await new this.productModel(payload).save();
  }
  async update(payload: Partial<ProductInterface>): Promise<boolean> {
    const { _id } = payload;
    const session: ClientSession = await this.productModel.db.startSession();
    try {
      session.startTransaction();
      await this.productModel.updateOne({ _id }, payload, { session });
      await session.commitTransaction();
      return true;
    } catch (err) {
      this.logger.log(err);
      session.abortTransaction();
      return false;
    }
  }
  async delete(payload: Partial<ProductInterface>): Promise<boolean> {
    const { _id } = payload;
    const session: ClientSession = await this.productModel.db.startSession();
    try {
      session.startTransaction();
      await this.productModel.deleteOne({ _id }, { session });
      await session.commitTransaction();
      return true;
    } catch (err) {
      this.logger.log(err);
      session.abortTransaction();
      return false;
    }
  }
}
