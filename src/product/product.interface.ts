import mongoose from 'mongoose';
import { Product } from './product.schemas';
export interface ProductInterface {
  _id?: string;
  article: string;
  name: string;
  price: number;
  created: number;
  medias: mongoose.Types.ObjectId[];
}

export interface ProductResponse {
  products: Product[];
  isMoreAvaliable: boolean;
}
