import {
  Controller,
  Get,
  Post,
  UseInterceptors,
  UploadedFiles,
  UseGuards,
} from '@nestjs/common';
import { MediaService } from './media.service';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { AuthGuard } from '@nestjs/passport';
import { uploadInterceptor } from './media.interceptor';

@Controller('/media')
export class MediaController {
  constructor(private readonly mediaService: MediaService) {}
  @Get('/')
  @UseGuards(AuthGuard('jwt'))
  medias() {
    return this.mediaService.getMedias();
  }
  @Post('/upload')
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(AnyFilesInterceptor(uploadInterceptor))
  upload(@UploadedFiles() files): any {
    return this.mediaService.uploadMedias(files);
  }
}
