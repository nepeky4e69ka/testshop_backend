import { HttpStatus, HttpException } from '@nestjs/common';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { v4 as uuid } from 'uuid';
import { config } from '../app.config';

export const uploadInterceptor = {
  storage: diskStorage({
    destination: config.static,
    fileSize: 5e7,
    files: 20,
    filename: (req: any, file: any, cb: any) => {
      void req;
      try {
        const fileName = uuid();
        return cb(null, `${fileName}${extname(file.originalname)}`);
      } catch (err) {
        return cb(
          new HttpException('Errored at upload', HttpStatus.BAD_REQUEST),
        );
      }
    },
  }),

  fileFilter: (req: any, file: any, cb: any) => {
    void req;
    config.allowedUploadFileTypes.includes(file.mimetype.toLowerCase())
      ? cb(null, true)
      : cb(
          new HttpException(
            `Unsupported file type: ${extname(file.mimetype)}`,
            HttpStatus.BAD_REQUEST,
          ),
          false,
        );
  },
};
