import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { Media } from './media.schemas';
import { MediaService } from './media.service';
import { MediaController } from './media.controller';

@Module({
  imports: [TypegooseModule.forFeature([Media])],
  providers: [MediaService],
  controllers: [MediaController],
  exports: [MediaService],
})
export class MediaModule {}
