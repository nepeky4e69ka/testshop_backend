import { Injectable } from '@nestjs/common';
import { MediaInterface, MediaEnum } from './media.interface';
import { Media } from './media.schemas';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';

@Injectable()
export class MediaService {
  constructor(
    @InjectModel(Media)
    private readonly mediaModel: ReturnModelType<typeof Media>,
  ) {}
  async uploadMedias(files: any): Promise<MediaInterface[]> {
    const medias = [];
    for (const file of files) {
      const filename = file.filename;
      const type = MediaEnum.PHOTO;
      const media = await this.createMedia({ filename, type });
      medias.push(media);
    }
    return medias;
  }
  async createMedia(media: MediaInterface): Promise<MediaInterface> {
    return await new this.mediaModel(media).save();
  }
  async getMedias(): Promise<MediaInterface[]> {
    return await this.mediaModel.find();
  }
}
