export interface MediaInterface {
  _id?: string;
  type: MediaType;
  filename: string;
}

export enum MediaEnum {
  PHOTO = 'PHOTO',
  VIDEO = 'VIDEO',
  AUDIO = 'AUDIO',
  DOCUMENT = 'DOCUMENT',
  VOICE = 'VOICE',
}
export type MediaType = 'PHOTO' | 'VIDEO' | 'AUDIO' | 'DOCUMENT' | 'VOICE';
