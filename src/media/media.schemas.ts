import { prop } from '@typegoose/typegoose';
import { MediaType } from './media.interface';
export class Media {
  @prop({ required: true })
  filename: string;

  @prop({ required: true })
  type: MediaType;
}
