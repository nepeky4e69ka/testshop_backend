## Installation

```bash
$ npm install
# required
$ npm install run-rs -g
```

## Running

```bash
# mongo
$ run-rs --shell

# development
$ npm run dev

```
